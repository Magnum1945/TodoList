﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using TodoList.DAL.Implementation;
using TodoList.DAL.Models;
using TodoList.MVC.Models;

namespace TodoList.MVC.Controllers
{
    [Authorize]
    public class TodoController : Controller
    {
        private UnitOfWork unitOfWork;
        private readonly IMapper mapper;

        public TodoController(ApplicationDbContext context, IMapper mapper)
        {
            this.unitOfWork = new UnitOfWork(context);
            this.mapper = mapper;
        }

        // GET: Todo
        public IActionResult Index()
        {
            var dbTodo = unitOfWork.TodoRepository.Get(x => x.UserName == User.Identity.Name);

            List<TodoViewModel> todoList = new List<TodoViewModel>();
            foreach (var item in dbTodo)
                todoList.Add(mapper.Map<Todo, TodoViewModel>(item));
            return View(todoList);
        }

        // GET: Todo/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var dbTodo = await unitOfWork.TodoRepository.GetByIDAsync(id);
            if (dbTodo == null)
            {
                return NotFound();
            }

            var todo = mapper.Map<Todo, TodoViewModel>(dbTodo);
            return View(todo);
        }

        // GET: Todo/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Todo/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,UserName,IsDone,Task,DueDate")] TodoViewModel todo)
        {
            if (ModelState.IsValid)
            {
                Todo dbTodo = mapper.Map<TodoViewModel, Todo>(todo);

                dbTodo.UserName = User.Identity.Name;
                await unitOfWork.TodoRepository.InsertAsync(dbTodo);
                await unitOfWork.SaveAsync();

                return RedirectToAction(nameof(Index));
            }
            return View(todo);
        }

        // GET: Todo/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var dbTodo = await unitOfWork.TodoRepository.GetByIDAsync(id);
            if (dbTodo == null)
            {
                return NotFound();
            }

            var todo = mapper.Map<Todo, TodoViewModel>(dbTodo);
            return View(todo);
        }

        // POST: Todo/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,UserName,IsDone,Task,DueDate")] TodoViewModel todo)
        {
            if (id != todo.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    Todo dbTodo = mapper.Map<TodoViewModel, Todo>(todo);

                    var databaseTodo = await unitOfWork.TodoRepository.GetByIDAsync(id);

                    dbTodo.UserName = User.Identity.Name;
                    unitOfWork.TodoRepository.Update(databaseTodo, dbTodo);
                    await unitOfWork.SaveAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!TodoExists(todo.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(todo);
        }

        // GET: Todo/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var dbTodo = await unitOfWork.TodoRepository.GetByIDAsync(id);
            if (dbTodo == null)
            {
                return NotFound();
            }

            var todo = mapper.Map<Todo, TodoViewModel>(dbTodo);
            return View(todo);
        }

        // POST: Todo/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var todo = await unitOfWork.TodoRepository.GetByIDAsync(id);
            unitOfWork.TodoRepository.Delete(todo);
            await unitOfWork.SaveAsync();

            return RedirectToAction(nameof(Index));
        }

        public IActionResult Welcome()
        {
            return View();
        }

        private bool TodoExists(int id)
        {
            return unitOfWork.TodoRepository.Get().ToList().Count == 0;
        }
    }
}
