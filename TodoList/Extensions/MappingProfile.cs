﻿using AutoMapper;
using TodoList.DAL.Models;
using TodoList.MVC.Models;

namespace TodoList.MVC.Extensions
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<Todo, TodoViewModel>();
        }
    }
}
