﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace TodoList.MVC.Models
{
    public class TodoViewModel
    {
        public int Id { get; set; }

        public string UserName { get; set; }

        [Display(Name = "Task done!")]
        public bool IsDone { get; set; }

        [Display(Name = "Task description")]
        [StringLength(60, MinimumLength = 3)]
        [Required]
        public string Task { get; set; }

        [Display(Name = "Due date")]
        [Required]
        public DateTime DueDate { get; set; }
    }
}
