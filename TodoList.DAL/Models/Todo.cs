﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace TodoList.DAL.Models
{
    public class Todo
    {
        public int Id { get; set; }
        public string UserName { get; set; }
        public bool IsDone { get; set; }
        public string Task { get; set; }
        public DateTime DueDate { get; set; }
    }
}
