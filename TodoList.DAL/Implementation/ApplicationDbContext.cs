﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using TodoList.DAL.Models;
using TodoList.DAL.Abstraction;
using Microsoft.EntityFrameworkCore.ChangeTracking;

namespace TodoList.DAL.Implementation
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>, ITodoListContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options)
        {
        }

        public DbSet<Todo> Todo { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            builder.Entity<Todo>().HasKey(x => x.Id);
        }

        EntityEntry ITodoListContext.Entry<T>(T entity)
        {
            return Entry<T>(entity);
        }

        async Task ITodoListContext.SaveChangesAsync()
        {
            await SaveChangesAsync();
        }

        DbSet<T> ITodoListContext.Set<T>()
        {
            return Set<T>();
        }
    }
}
