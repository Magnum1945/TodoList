﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using TodoList.DAL.Abstraction;

namespace TodoList.DAL.Implementation
{
    public class GenericRepository<TEntity> : IGenericRepository<TEntity> where TEntity : class
    {
        private ITodoListContext context;
        public GenericRepository(ITodoListContext context)
        {
            this.context = context;
        }
        public virtual IQueryable<TEntity> Get(Expression<Func<TEntity, bool>> filter = null,
                                               Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null,
                                               string includeProperties = "")
        {
            IQueryable<TEntity> query = context.Set<TEntity>().AsQueryable();
            if (filter != null)
            {
                query = query.Where(filter);
            }
            foreach (var includeProperty in includeProperties.Split
                (new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
            {
                query = query.Include(includeProperty);
            }
            if (orderBy != null)
            {
                return orderBy(query).ToList().AsQueryable();
            }
            else
            {
                return query.ToList().AsQueryable();
            }
        }
        public async virtual Task<TEntity> GetByIDAsync(object id)
        {
            return await context.Set<TEntity>().FindAsync(id);
        }
        public async virtual Task InsertAsync(TEntity entity)
        {
            await context.Set<TEntity>().AddAsync(entity);
        }
        public async virtual Task DeleteAsync(object id)
        {
            TEntity entityToDelete = await context.Set<TEntity>().FindAsync(id);
            Delete(entityToDelete);
        }
        public virtual void Delete(TEntity entityToDelete)
        {
            context.Set<TEntity>().Remove(entityToDelete);
        }
        public virtual void Update(TEntity entityToUpdate)
        {
            context.Set<TEntity>().Attach(entityToUpdate);
            context.Entry(entityToUpdate).State = EntityState.Modified;
        }
        public virtual void Update(TEntity entityFromDatabase, TEntity entityToUpdate)
        {
            context.Entry(entityFromDatabase).CurrentValues.SetValues(entityToUpdate);
        }
    }

}
