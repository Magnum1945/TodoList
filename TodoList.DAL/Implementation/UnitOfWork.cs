﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;
using TodoList.DAL.Abstraction;
using TodoList.DAL.Models;

namespace TodoList.DAL.Implementation
{
    public class UnitOfWork : IUnitOfWork
    {
        private ITodoListContext context;
        private GenericRepository<Todo> todoRepository;
        private bool disposed = false;

        public UnitOfWork(ITodoListContext context)
        {
            this.context = context;
        }

        public GenericRepository<Todo> TodoRepository
        {
            get
            {
                if (todoRepository == null)
                    todoRepository = new GenericRepository<Todo>(context);
                return todoRepository;
            }
        }

        public async Task SaveAsync()
        {
            bool saveFailed;
            do
            {
                saveFailed = false;
                try
                {
                    await context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException ex)
                {
                    saveFailed = true;
                    ex.Entries.Single().Reload();
                }
            } while (saveFailed);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    context.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
