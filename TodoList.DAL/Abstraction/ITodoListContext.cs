﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using TodoList.DAL.Models;
using System.Threading.Tasks;

namespace TodoList.DAL.Abstraction
{
    public interface ITodoListContext : IDisposable
    {
        DbSet<Todo> Todo { get; set; }

        Task SaveChangesAsync();

        DbSet<T> Set<T>() where T : class;

        EntityEntry Entry<T>(T entity) where T : class;
    }
}
