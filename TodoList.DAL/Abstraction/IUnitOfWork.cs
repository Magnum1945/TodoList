﻿using System;
using System.Threading.Tasks;
using TodoList.DAL.Implementation;
using TodoList.DAL.Models;

namespace TodoList.DAL.Abstraction
{
    public interface IUnitOfWork : IDisposable
    {
        GenericRepository<Todo> TodoRepository { get; }

        Task SaveAsync();
    }
}
